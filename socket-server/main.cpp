#include <iostream>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <cstring>
using namespace std;

# define SERVER_PORT 2468
# define BUFFER_LEN 256
# define QUEUE_LEN 10

int main() {
    int server_socket_fd, client_socket_fd;
    struct sockaddr_in server_addr, client_addr;
    char buffer[BUFFER_LEN];

    server_socket_fd = socket(AF_INET, SOCK_STREAM, 0);

    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_port = htons(SERVER_PORT);

    bind(server_socket_fd, (struct sockaddr *) &server_addr, sizeof(server_addr));
    listen(server_socket_fd, QUEUE_LEN);
        
    while(1) {
	bzero(buffer, sizeof(buffer));
        int client_addr_size = sizeof(client_addr);
	client_socket_fd = accept(server_socket_fd, (struct sockaddr *) &client_addr, (socklen_t *) &client_addr_size);
	int count_bytes = recv(client_socket_fd, buffer, sizeof(buffer), 0);
        cout << "Count bytes: " << count_bytes << endl;
	cout << "String: " << buffer << endl;
	close(client_socket_fd);
    }

    close(server_socket_fd);
}